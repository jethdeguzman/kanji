<!DOCTYPE HTML>
<!-- BEGIN html -->
<html lang="en">
  <head>
    <title>Kanji Hybrid</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="name" content="@yield('meta-title')">
    <meta name="description" content="@yield('meta-description')">
    <meta name="tags" content="@yield('meta-tags')">
    <meta name="author" content="Jeff Simons Decena">
    <link rel="shortcut icon" href="{{ asset('themes/front/img/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('themes/front/img/favicon.ico')}}" type="image/x-icon">

   
	
	@section('header')
		{{ HTML::style('themes/front/fraction/css/reset.css') }}
    {{ HTML::style('themes/front/fraction/css/font-awesome.min.css') }}
    {{ HTML::style('themes/front/fraction/css/animate.css') }}
    {{ HTML::style('themes/front/fraction/css/main-stylesheet.css') }}
    {{ HTML::style('themes/front/fraction/css/lightbox.css') }}
    {{ HTML::style('themes/front/fraction/css/shortcodes.css') }}

    {{ HTML::style('themes/front/fraction/css/custom-colors.css') }}
    {{ HTML::style('themes/front/fraction/css/responsive.css') }}
    {{ HTML::style('themes/front/fraction/css/owl.carousel.css') }}
    {{ HTML::style('themes/front/fraction/css/owl.theme.css') }}
    {{ HTML::style('themes/front/fraction/css/dat-menu.css') }}
		
    {{ HTML::style('themes/front/fraction/css/demo-settings.css') }}

    {{ HTML::script('themes/front/fraction/js/jquery-latest.min.js') }}

    <style type="text/css">
    .kanji-wrap{
      color:#999;
    }
    .kanji-wrap p{
      color:#999;
    }
    .kanji-wrap p strong, .kanji-wrap p b{
      color: #494949;
    }
    .kanji-wrap strong, .kanji-wrap b{
      color: #494949;
    }
    .kanji-wrap p em{
      display:inline-block; 
      font-style: normal;
    }
    .kanji-wrap p em > em{
      font-style: italic;
    }
    .kanji-wrap p span{
      display:inline-block; 
      font-style: normal;
    }
    </style>
	@show
  </head>

  <body>
    <a href="#dat-menu" class="ot-menu-toggle"><i class="fa fa-bars"></i>Toggle Menu</a>
    <!-- BEGIN .boxed -->
    <div class="boxed active">
      @include('front.fraction.header')

      @yield('body')
      
      @include('front.fraction.footer')
    </div> <!-- /container -->


    {{ HTML::script('themes/front/fraction/js/snap.svg-min.js') }} 
    {{ HTML::script('themes/front/fraction/js/theme-scripts.js') }} 
    {{ HTML::script('themes/front/fraction/js/lightbox.js') }} 
    {{ HTML::script('themes/front/fraction/js/owl.carousel.min.js') }} 
    {{ HTML::script('themes/front/fraction/js/SmoothScroll.min.js') }} 
    {{ HTML::script('themes/front/fraction/js/iscroll.js') }} 
    {{ HTML::script('themes/front/fraction/js/modernizr.custom.50878.js') }} 
    {{ HTML::script('themes/front/fraction/js/dat-menu.js') }}  
    
    <script>
      jQuery(document).ready(function() {
        jQuery(".ot-slider").owlCarousel({
          items : 1,
          autoPlay : true,
          stopOnHover : true,
          navigation : true,
          lazyLoad : true,
          singleItem : true,
          pagination : false
        });
      });
    </script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->   
  </body>
</html>