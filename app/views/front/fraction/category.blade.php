@extends('front.fraction.tpl.main')

@section('meta-title')
{{ ucwords(Request::segment(1)) }}
@stop

@section('body')
<!-- BEGIN .content -->
<section class="content">
	
	<!-- BEGIN .wrapper -->
	<div class="wrapper">
		


		<!-- <div class="main-content has-sidebar"> -->
		<div class="main-content has-double-sidebar">

			<!-- BEGIN .left-content -->
			<div class="left-content">
			@if(isset($posts) && !empty($posts) && ! $posts->isEmpty())
				<div class="main-title" style="border-left: 4px solid #276197;">
					<h2>Latest {{ $category->category }}</h2>
					<span>{{$category->description}}</span>
				</div>
				
				<div class="article-list">
				@foreach( $posts as $post )
					<div class="item">
						<div class="item-header">
							<a href="post.html" class="image-hover" data-path-hover="M 41,53 46,34 56.5,37.5 58,59 48.5,68.2 z">
								<figure>
									@if(!empty($post->img_src))
										<img src="{{ asset("uploads/$post->img_src") }}"/>
									@else
										<img src="/themes/front/fraction/images/photos/image-14.jpg" />
									@endif
									<svg viewBox="0 0 100 100" preserveAspectRatio="none"><path d="M 0,100 0,99 50,97 100,99 100,100 z" fill="#276197" /></svg>
									<figcaption>
										<span class="hover-text"><i class="fa fa-camera"></i><span>Photo</span></span>
									</figcaption>
								</figure>
							</a>
						</div>
						<div class="item-content">
							
							<h3><a href="{{ URL::to($post->slug) }}">{{ $post->title }}</a></h3>
							<p><i class="fa fa-calendar"></i> Posted on {{ date('F d Y', strtotime($post->created_at)) }}</p>
							<!-- <div class="ot-star-rating">
								<span style="width: 100%;" class=""><strong class="rating">5</strong> out of 5</span>
								<strong>Rating: 5 out of 5 stars</strong>
							</div> -->
							<span class="kanji-wrap">{{ str_limit($post->content, 250, ' ...') }}</span>
							
						</div>
						<a href="{{ URL::to($post->slug) }}" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
					</div>
				@endforeach
				</div>

				@if($posts->getLastPage() > 1)
				<div class="pagination">
				
					@if($posts->getCurrentPage() > 1)
						<a class="prev page-numbers" href="?page={{$posts->getCurrentPage() - 1}}"><i class="fa fa-caret-left"></i>Previous Page</a>
					@endif
					@for($i = 1; $i<= $posts->getLastPage(); $i++ )
						<a class="page-numbers @if(Input::get('page') == $i) current @endif" href="?page={{$i}}">{{$i}}</a>
					@endfor
					@if($posts->getCurrentPage() != $posts->getLastPage())
						<a class="next page-numbers" href="?page={{$posts->getCurrentPage() + 1}}">Next Page<i class="fa fa-caret-right"></i></a>
					@endif
					
				</div>
				@endif
			@else
				<p class="alert alert-danger">Sorry, no posts to show.</p>
			@endif
			<!-- END .left-content -->
			</div>x

			<!-- BEGIN .small-sidebar -->
			<div class="small-sidebar">
				
				<div class="widget">
					<h3 style="color: #e14420; border-bottom: 3px solid #e14420;">Latest News</h3>
					<div class="article-block">
						<div class="item no-image">
							<div class="item-content">
								<h4><a href="post.html">Simul officiis ad vim ius et detracto</a></h4>
								<p>Lorem ipsum dolor sit amet, inimicus definias illum partem diceret meis suscipiantur...</p>
								<span><a href="blog.html">September 11, 2014</a></span>
							</div>
						</div>
						<div class="item no-image">
							<div class="item-content">
								<h4><a href="post.html">Simul officiis ad vim ius et detracto</a></h4>
								<p>Lorem ipsum dolor sit amet, inimicus definias illum partem diceret meis suscipiantur...</p>
								<span><a href="blog.html">September 11, 2014</a></span>
							</div>
						</div>
						<div class="item no-image">
							<div class="item-content">
								<h4><a href="post.html">Simul officiis ad vim ius et detracto</a></h4>
								<p>Lorem ipsum dolor sit amet, inimicus definias illum partem diceret meis suscipiantur...</p>
								<span><a href="blog.html">September 11, 2014</a></span>
							</div>
						</div>
					</div>
				</div>

				<div class="widget">
					<div class="banner">
						<a href="#" target="_blank"><img src="/themes/front/fraction/images/no-banner-160x600.jpg" alt="" /></a>
					</div>
				</div>

				<div class="widget">
					<h3 style="color: #276197; border-bottom: 3px solid #276197;">Tech Reviews</h3>
					<div class="article-block reviews">
						<div class="item">
							<div class="item-header">
								<a href="post-review.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-10.jpg" alt="" /></a>
							</div>
							<div class="item-content">
								<h4><a href="post-review.html">Simul officiis ad vim ius et detracto</a></h4>
								<div class="ot-star-rating">
									<span style="width: 100%;" class=""><strong class="rating">5</strong> out of 5</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="item-header">
								<a href="post-review.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-11.jpg" alt="" /></a>
							</div>
							<div class="item-content">
								<h4><a href="post-review.html">Simul officiis ad vim ius et detracto</a></h4>
								<div class="ot-star-rating">
									<span style="width: 70%;" class=""><strong class="rating">3.5</strong> out of 5</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="item-header">
								<a href="post-review.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-12.jpg" alt="" /></a>
							</div>
							<div class="item-content">
								<h4><a href="post-review.html">Simul officiis ad vim ius et detracto</a></h4>
								<div class="ot-star-rating">
									<span style="width: 80%;" class=""><strong class="rating">4</strong> out of 5</span>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="item-header">
								<a href="post-review.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-13.jpg" alt="" /></a>
							</div>
							<div class="item-content">
								<h4><a href="post-review.html">Simul officiis ad vim ius et detracto</a></h4>
								<div class="ot-star-rating">
									<span style="width: 50%;" class=""><strong class="rating">2.5</strong> out of 5</span>
								</div>
							</div>
						</div>
					</div>
				</div>

			<!-- END .small-sidebar -->
			</div>

			@include('front.fraction.sidebar')

		</div>
		
	<!-- END .wrapper -->
	</div>
	
<!-- BEGIN .content -->
</section>
<script>
	jQuery(function(){

		$('.kanji-wrap').each(function(){

			var content = "";
			$(this).find('p').each(function()
			{
				
				var value = $(this).html();
				var regex = /\s+/gi;
				var words = value.trim().replace(regex, ' ').split(' ');
				var wrapped ="";
				for (var i in words) {
					wrapped = wrapped + "<span> "+words[i]+" </span>&nbsp;";
				}
				content = content + "<p>"+wrapped+"</p>";
				
			});
			
			$(this).html(content);
		});
		
	});
</script>
@stop