<!-- BEGIN .footer -->
<footer class="footer">
	
	<!-- BEGIN .wrapper -->
	<div class="wrapper">

		<div class="footer-widgets">

			<!-- BEGIN .widget -->
			<div class="widget">
				<h3>About Fraction</h3>
				<div class="socialize-widget">
					<p>Lorem ipsum dolor sit amet, inimicus at et viets, illum partem diceret. Meis suscipiantur mel cu. Debitis prodesset definiebas, dictas posidonium te, oratio omnesque hendrerit usu cu.</p>
					<ul class="list-group">
						<li><i class="fa fa-location-arrow fa-fw"></i>122 Baker Street, Marylebone<br/>London, W1U 6TX</li>
						<li><i class="fa fa-phone fa-fw"></i>0870 241 3300</li>
						<li><i class="fa fa-envelope fa-fw"></i>support@theme.com</li>
					</ul>
				</div>
			<!-- END .widget -->
			</div>

			<!-- BEGIN .widget -->
			<div class="widget" style="background: red;">
				<div class="ot-tabbed">
					<h3 class="active">Comments</h3>
					<h3>Latest News</h3>
				</div>
				<div class="comments-block ot-tab-block active">
					<div class="item">
						<div class="item-header">
							<a href="#" class="image-avatar"><img src="/themes/front/fraction/images/photos/avatar-1.jpg" alt="" /></a>
						</div>
						<div class="item-content">
							<h4><a href="#">Orange-Themes</a></h4>
							<p>Cum an officiis integebat necessitatibus, impedi tes menandri has clita...</p>
							<a href="post.html" class="read-more-link">View article<i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
					<div class="item">
						<div class="item-header">
							<a href="#" class="image-avatar"><img src="/themes/front/fraction/images/photos/avatar-2.jpg" alt="" /></a>
						</div>
						<div class="item-content">
							<h4><a href="#">Keir Donato</a></h4>
							<p>Simul albucius accusata per in, ei soleat insolens.</p>
							<a href="post.html" class="read-more-link">View article<i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<div class="article-block ot-tab-block">
					<div class="item">
						<div class="item-header">
							<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-17.jpg" alt="" /></a>
						</div>
						<div class="item-content">
							<div class="content-category">
								<a href="category.html" style="color: #276197;">Tech News</a>
							</div>
							<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
							<span><a href="blog.html">September 11, 2014</a></span>
						</div>
					</div>
					<div class="item">
						<div class="item-header">
							<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-19.jpg" alt="" /></a>
						</div>
						<div class="item-content">
							<div class="content-category">
								<a href="category.html" style="color: #1985e1;">Nostalgia</a>
							</div>
							<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
							<span><a href="blog.html">September 11, 2014</a></span>
						</div>
					</div>
					<div class="item">
						<div class="item-header">
							<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-18.jpg" alt="" /></a>
						</div>
						<div class="item-content">
							<div class="content-category">
								<a href="category.html" style="color: #ef8722;">World News</a>
							</div>
							<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
							<span><a href="blog.html">September 11, 2014</a></span>
						</div>
					</div>
				</div>
			<!-- END .widget -->
			</div>

			<!-- BEGIN .widget -->
			<div class="widget">
				<h3>Tag Cloud</h3>
				<div class="tagcloud">
					<a href="category.html">Aeterno</a>
					<a href="category.html">Definitionem</a>
					<a href="category.html">Melius</a>
					<a href="category.html">meis etiam volutpat at nam</a>
					<a href="category.html">nec nibh</a>
					<a href="category.html">Persequeris</a>
					<a href="category.html">vel</a>
					<a href="category.html">Nominavi</a>
					<a href="category.html">Aeterno</a>
					<a href="category.html">Definitionem</a>
					<a href="category.html">Melius</a>
					<a href="category.html">meis etiam volutpat</a>
					<a href="category.html">nec nibh</a>
					<a href="category.html">Persequeris</a>
				</div>
			<!-- END .widget -->
			</div>

		</div>
		
	<!-- END .wrapper -->
	</div>

	<div class="footer-copyright">
		<!-- BEGIN .wrapper -->
		<div class="wrapper">
			<p class="right">Designed by <strong><a href="http://orange-themes.com" target="_blank">Orange-Themes.com</a></strong></p>
			<p>&copy; All Rights Reserved <strong>Fraction Magazine</strong></p>
		<!-- END .wrapper -->
		</div>
	</div>
	
<!-- END .footer -->
</footer>