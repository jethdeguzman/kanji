<!-- BEGIN .header -->
<header class="header willfix">
  
  <!-- BEGIN .wrapper -->
  <div class="wrapper">

    <div class="header-left">
      <div class="header-logo">

        <a href="/" class="otanimation" data-anim-object=".header-logo a.otanimation img, .header-logo a.otanimation h1" data-anim-in="flipOutX" data-anim-out="bounceIn" style="font-size:28px;">KanjiHybrid</a>
     <!--    <strong data-anim-in="fadeOutUpBig" data-anim-out="bounceIn"><i class="fa fa-home"></i> Homepage</strong> -->
      </div>
      <div class="header-socials">
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-youtube"></i></a>
        <a href="#"><i class="fa fa-vimeo-square"></i></a>
      </div>
    </div>

    <div class="header-right">
      <nav class="main-menu">
        <ul class="load-responsive" rel="Main Menu">
          <li><a href="blog.html">Latest News<i>Blog Layout</i></a></li>
          <li><a href="shortcodes.html"><span>Features<i>Theme features</i></span></a>
            <ul class="sub-menu">
              <li><a href="shortcodes.html">Shortcodes</a></li>
              <li><a href="category.html"><span>Custom Templates</span></a>
                <ul class="sub-menu">
                  <li><a href="category.html">Category layout</a></li>
                  <li><a href="photo-gallery.html">Photo Galleries</a></li>
                  <li><a href="photo-gallery-lightbox.html">Lightbox Galleries</a></li>
                  <li><a href="error-404.html">404 Page</a></li>
                  <li><a href="contact-us.html">Contact Us</a></li>
                  <li><a href="archive.html">Archive</a></li>
                </ul>
              </li>
              <li><a href="full-width.html">Full-width page</a></li>
            </ul>
          </li>
          <li class="has-ot-mega-menu"><a href="#"><span>Mega Menu<i>Custom Template</i></span></a>
            <ul class="ot-mega-menu">
              <li class="ot-dropdown menu-item">
                <ul class="widget-menu widget">
                  <li><a href="category.html"><i class="fa fa-comment"></i>Putent tibique antiopam</a></li>
                  <li><a href="category.html"><i class="fa fa-camera-retro"></i>Inani platonem constituto</a></li>
                  <li><a href="category.html"><i class="fa fa-bomb"></i>Mutat libris sea et sea</a></li>
                  <li><a href="category.html"><i class="fa fa-fighter-jet"></i>Eos nisl possim molestiae ex</a></li>
                  <li><a href="category.html"><i class="fa fa-support"></i>Ut quem petentium has</a></li>
                </ul>
              </li>
              <li class="ot-dropdown menu-item">
                <ul class="widget">
                  <li>
                    <div class="article-block">
                      <div class="item featured">
                        <div class="item-header">
                          <a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-1.jpg" alt="" /></a>
                        </div>
                        <div class="item-content">
                          <h4><a href="post.html">Habeo iudicabit mea in no elitr maiorum</a></h4>
                          <p>Mea an vitae iudico. Ad eum dicam everti, id vis probo habeo scripta...</p>
                          <span>September 11, 2014</span>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-header">
                          <a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-2.jpg" alt="" /></a>
                        </div>
                        <div class="item-content">
                          <h4><a href="post.html">Habeo iudicabit mea in no elitr maiorum sit</a></h4>
                          <span>September 11, 2014</span>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li class="ot-dropdown menu-item">
                <ul class="widget">
                  <li>
                    <div class="article-block">
                      <div class="item">
                        <div class="item-header">
                          <a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-3.jpg" alt="" /></a>
                        </div>
                        <div class="item-content">
                          <h4><a href="post.html">Habeo iudicabit mea in no elitr maiorum sit</a></h4>
                          <span>September 11, 2014</span>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-header">
                          <a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-4.jpg" alt="" /></a>
                        </div>
                        <div class="item-content">
                          <h4><a href="post.html">Vidisse mandamus iudicabit ex cum suavitate complectitur sit</a></h4>
                          <span>September 11, 2014</span>
                        </div>
                      </div>
                      <div class="item">
                        <div class="item-header">
                          <a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-5.jpg" alt="" /></a>
                        </div>
                        <div class="item-content">
                          <h4><a href="post.html">Posse labores detraxit malis eirmod scripta per salutandi</a></h4>
                          <span>September 11, 2014</span>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="has-ot-mega-menu"><a href="#"><span>Widget Menu<i>Widgetized Mega Menu</i></span></a>
            <ul class="ot-mega-menu">
              <li>
                <div>
                  <div class="widget">
                    <h3>Subscribe Newsletter</h3>
                    <div class="subscribe-feed">
                      <p>Te lorem libris iracundia eos. Ne eam liber veritus, eos eu agam recteque, exerci reformidans sea no.</p>

                      <!-- <div class="coloralert aweber-success">
                        <i class="fa fa-check"></i>
                        <p>Success! Everything went well, You are now subscribed !</p>
                        <a href="#close-alert"><i class="fa fa-times-circle"></i></a>
                      </div> -->

                      <!-- <div class="coloralert aweber-fail">
                        <i class="fa fa-warning"></i>
                        <p>Error Occurred!</p>
                        <a href="#close-alert"><i class="fa fa-times-circle"></i></a>
                      </div> -->

                      <div class="coloralert aweber-loading">
                        <i class="fa fa-refresh fa-spin"></i>
                        <p>Loading.. <br>This may take a few seconds !</p>
                        <a href="#close-alert"><i class="fa fa-times-circle"></i></a>
                      </div>

                      <form name="aweber-form" action="#" class="subscribe-form aweber-form">
                        <p class="sub-email">
                          <input type="text" value="" placeholder="E-mail address" name="email" class="email">
                        </p>
                        <p class="sub-submit">
                          <input type="submit" value="Subscribe" class="button aweber-submit">
                        </p>
                      </form>
                    </div>
                  </div>
                  <div class="widget">
                    <div class="banner">
                      <a href="#" target="_blank"><img src="/themes/front/fraction/images/no-banner-300x250.jpg" alt="" /></a>
                    </div>
                  </div>
                  <!-- BEGIN .widget -->
                  <div class="widget">
                    <h3>Tag Cloud</h3>
                    <div class="tagcloud">
                      <a href="category.html">Aeterno</a>
                      <a href="category.html">Definitionem</a>
                      <a href="category.html">Melius</a>
                      <a href="category.html">meis etiam volutpat at nam</a>
                      <a href="category.html">nec nibh</a>
                      <a href="category.html">Persequeris</a>
                      <a href="category.html">vel</a>
                      <a href="category.html">Nominavi</a>
                      <a href="category.html">Aeterno</a>
                      <a href="category.html">Definitionem</a>
                      <a href="category.html">Melius</a>
                      <a href="category.html">meis etiam volutpat</a>
                      <a href="category.html">nec nibh</a>
                    </div>
                  <!-- END .widget -->
                  </div>
                </div>
              </li>
            </ul>
          </li>
          <li><a href="contact-us.html">Contact Us<i>Get in touch</i></a></li>
        </ul>
        <div class="search-header">
          <form action="get">
            <input type="search" value="" placeholder="Search.." autocomplete="off" required="required" name="s" />
            <input type="submit" value="search" />
          </form>
        </div>
      </nav>
      <nav class="under-menu">
        <ul class="load-responsive" rel="Sub Menu">
          <li><a href="blog.html"><span>Blog Layout</span></a>
            <ul>
              <li><a href="blog.html">Blog Default</a></li>
              <li><a href="blog-reviews.html">Blog Reviews</a></li>
              <li><a href="blog-youtube.html">Blog Youtube</a></li>
              <li><a href="blog-vimeo.html">Blog Vimeo</a></li>
              <li><a href="blog-soundcloud.html">Blog Soundcloud</a></li>
              <li><a href="blog-mixcloud.html">Blog Mixcloud</a></li>
              <li><a href="category.html">Category Layout</a></li>
            </ul>
          </li>
          <li><a href="post.html"><span>Post Layouts</span></a>
            <ul>
              <li><a href="post-simple.html">Simple Post</a></li>
              <li><a href="post.html"><span>Image Post</span></a>
                <ul>
                  <li><a href="post-no-comments.html">Post No Comments</a></li>
                  <li><a href="post-sidebar-no.html">Post No Sidebar</a></li>
                  <li><a href="post-sidebar-double.html">Post Double Sidebar</a></li>
                  <li><a href="post-review.html">Post Review</a></li>
                </ul>
              </li>
              <li><a href="post-youtube.html">Youtube Post</a></li>
              <li><a href="post-vimeo.html">Vimeo Post</a></li>
              <li><a href="post-soundcloud.html">Soundcloud Post</a></li>
              <li><a href="post-mixcloud.html">Mixcloud Post</a></li>
            </ul>
          </li>
          <li><a href="photo-gallery.html"><span>Photo Gallery</span></a>
            <ul>
              <li><a href="photo-gallery-single.html">Photo Gallery Single</a></li>
              <li><a href="photo-gallery-lightbox.html">Photo Gallery Lightbox</a></li>
            </ul>
          </li>
          <li><a href="archive.html">Archive</a></li>
          <li><a href="error-404.html">404 Page</a></li>
          <li><a href="contact-us.html">Contact Us</a></li>
        </ul>
      </nav>
    </div>

    <div class="clear-float"></div>
    
  <!-- END .wrapper -->
  </div>
  
<!-- END .header -->
</header>