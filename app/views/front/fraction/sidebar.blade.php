<!-- BEGIN #sidebar -->
<aside id="sidebar">

	<div class="widget">
		<div class="banner">
			<a href="#" target="_blank"><img src="/themes/front/fraction/images/no-banner-300x250.jpg" alt="" /></a>
		</div>
	</div>
	
	<div class="widget">
		<h3>Popular Articles</h3>
		<div class="article-block">
			<div class="item">
				<div class="item-header">
					<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-17.jpg" alt="" /></a>
				</div>
				<div class="item-content">
					<div class="content-category">
						<a href="category.html" style="color: #276197;">Tech News</a>
					</div>
					<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
					<span><a href="blog.html">September 11, 2014</a></span>
				</div>
			</div>
			<div class="item">
				<div class="item-header">
					<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-18.jpg" alt="" /></a>
				</div>
				<div class="item-content">
					<div class="content-category">
						<a href="category.html" style="color: #ef8722;">World News</a>
					</div>
					<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
					<span><a href="blog.html">September 11, 2014</a></span>
				</div>
			</div>
			<div class="item">
				<div class="item-header">
					<a href="post.html" class="image-hover"><img src="/themes/front/fraction/images/photos/image-19.jpg" alt="" /></a>
				</div>
				<div class="item-content">
					<div class="content-category">
						<a href="category.html" style="color: #1985e1;">Nostalgia</a>
					</div>
					<h4><a href="post.html">Eum ex civibus pertinax antur his ea dicam</a></h4>
					<span><a href="blog.html">September 11, 2014</a></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="widget">
		<h3>Subscribe Newsletter</h3>
		<div class="subscribe-feed">
			<p>Te lorem libris iracundia eos. Ne eam liber veritus, eos eu agam recteque, exerci reformidans sea no.</p>

			<!-- <div class="coloralert aweber-success">
				<i class="fa fa-check"></i>
				<p>Success! Everything went well, You are now subscribed !</p>
				<a href="#close-alert"><i class="fa fa-times-circle"></i></a>
			</div> -->

			<!-- <div class="coloralert aweber-fail">
				<i class="fa fa-warning"></i>
				<p>Error Occurred!</p>
				<a href="#close-alert"><i class="fa fa-times-circle"></i></a>
			</div> -->

			<div class="coloralert aweber-loading">
				<i class="fa fa-refresh fa-spin"></i>
				<p>Loading.. <br>This may take a few seconds !</p>
				<a href="#close-alert"><i class="fa fa-times-circle"></i></a>
			</div>

			<form name="aweber-form" action="#" class="subscribe-form aweber-form">
				<p class="sub-email">
					<input type="text" value="" placeholder="E-mail address" name="email" class="email">
				</p>
				<p class="sub-submit">
					<input type="submit" value="Subscribe" class="button aweber-submit">
				</p>
			</form>
		</div>
	</div>
	
	


<!-- END #sidebar -->
</aside>