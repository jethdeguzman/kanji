@extends('front.fraction.tpl.main')

@section('body')
<!-- BEGIN .content -->
<section class="content">
	
	<!-- BEGIN .wrapper -->
	<div class="wrapper">
		

		<!-- BEGIN .breaking-news -->
		<div class="breaking-news">
			<div class="breaking-title">
				<h3>Breaking News</h3>
			</div>
			<div class="breaking-block">
				<ul>
					<li><a href="category.html" class="break-category" style="background-color: #276197;">Tech News</a><h4><a href="post.html">Prize pool for The International 2014 surges to $5.7 million</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
					<li><a href="category.html" class="break-category" style="background-color: #429d4a;">Contests</a><h4><a href="post.html">An rebum nusquam cum duo invenire mundi</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
					<li><a href="category.html" class="break-category" style="background-color: #ffd800; color: #232323;">World News</a><h4><a href="post.html">An rebum nusquam cum duo invenire mundi</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
				</ul>
			</div>
		<!-- END .breaking-news -->
		</div>


		<div class="main-content has-sidebar">
		<!-- <div class="main-content has-double-sidebar"> -->

			<!-- BEGIN .left-content -->
			<div class="left-content">
				@if( isset($record) && $record )
				<div class="article-content">
					
					<div class="article-header">
						<div class="content-category">
							@foreach(Posts::find($record->id_post)->categories as $category)
								<a href="{{ URL::to('categories/'.$category->slug) }}" style="color: #276197;">{{$category->category}}</a>
							@endforeach
						</div>
						<h1>{{ $record->title }}</h1>
						<span>
							
							<span><a href="#">{{ date('F d Y', strtotime($record->created_at)) }}</a></span>
						
						</span>
					</div>
					<span style="margin-right:10px; float:left;">
					@if(!empty($record->img_src))
						<img src="{{ asset("uploads/$record->img_src") }}"  style="height:150px; "/>
					@else
						<img src="/themes/front/fraction/images/photos/image-58.jpg" />
					@endif
					</span>
					<span class="kanji-wrap" >
						{{ $record->content }}
					</span>
				</div>

				

				<div class="share-article-body">
					<div class="main-title">
						<h2>Share This Article</h2>
						<span>Do the sharing thingy</span>
					</div>
					<div class="right">
						<a href="#" class="share-body ot-facebook"><i class="fa fa-facebook"></i><span>2.3k</span></a>
						<a href="#" class="share-body ot-twitter"><i class="fa fa-twitter"></i><span>2.3k</span></a>
						<a href="#" class="share-body ot-google"><i class="fa fa-google-plus"></i><span>2.3k</span></a>
						<a href="#" class="share-body ot-linkedin"><i class="fa fa-linkedin"></i><span>2.3k</span></a>
						<a href="#" class="share-body ot-pinterest"><i class="fa fa-pinterest"></i><span>2.3k</span></a>
					</div>
				</div>

				@else
					<p class="alert alert-danger">Sorry, we cannot find the page you are looking. Please try again.</p>
				@endif
				<div class="article-body-banner banner">
					<a href="#" target="_blank"><img src="/themes/front/fraction/images/no-banner-728x90.jpg" alt="" /></a>
				</div>

			<!-- END .left-content -->
			</div>

			@include('front.fraction.sidebar')

		</div>
		
	<!-- END .wrapper -->
	</div>
	
<!-- BEGIN .content -->
</section>

<script>
	jQuery(function(){

		$('.kanji-wrap').each(function(){

			var content = "";
			$(this).find('p').each(function()
			{
				
				var value = $(this).html();
				var regex = /\s+/gi;
				var words = value.trim().replace(regex, ' ').split(' ');
				var wrapped ="";
				for (var i in words) {
					wrapped = wrapped + "<em>"+words[i]+"</em>&nbsp;";
				}
				content = content + "<p>"+wrapped+"</p>";
				
			});
			
			$(this).html(content);
		});
		
	});
</script>
@stop

