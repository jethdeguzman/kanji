@extends('front.fraction.tpl.main')

@section('meta-title') Kanji Hybrid @stop

@section('meta-description') Open source CMS project made in the Philippines @stop

@section('meta-tags') open source, cms, philippines @stop

@section('body')
<section class="content">
	
	<!-- BEGIN .wrapper -->
	<div class="wrapper">
		

		<!-- <div class="breaking-news">
			<div class="breaking-title">
				<h3>Breaking News</h3>
			</div>
			<div class="breaking-block">
				<ul>
					<li><a href="category.html" class="break-category" style="background-color: #276197;">Tech News</a><h4><a href="post.html">Prize pool for The International 2014 surges to $5.7 million</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
					<li><a href="category.html" class="break-category" style="background-color: #429d4a;">Contests</a><h4><a href="post.html">An rebum nusquam cum duo invenire mundi</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
					<li><a href="category.html" class="break-category" style="background-color: #ffd800; color: #232323;">World News</a><h4><a href="post.html">An rebum nusquam cum duo invenire mundi</a></h4><a href="post.html#comments" class="comment-link"><i class="fa fa-comment-o"></i>0</a></li>
				</ul>
			</div>
		</div> -->


		<div class="main-content has-sidebar">
		<!-- <div class="main-content has-double-sidebar"> -->
		<!-- <div class="main-content"> -->

			<!-- BEGIN .ot-slider -->
			<div class="ot-slider owl-carousel">

				<!-- BEGIN .ot-slide -->
				<div class="ot-slide">
					<div class="ot-slider-layer first">
						<a href="post.html">
							<strong><i style="background-color: #ed2d00; color: #fff;">Breaking News</i>a fight club on the top of the world</strong>
							<img src="/themes/front/fraction/images/photos/image-6.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer second">
						<a href="post.html">
							<strong><i style="background-color: #ffd800; color: #232323;">World News</i>SAMENWERKING GOLIATH SPORTSWEAR &amp; TWEE NEDERLANDSE PINTEREST INFLUENCERS</strong>
							<img src="/themes/front/fraction/images/photos/image-7.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer third">
						<a href="post.html">
							<strong><i style="background-color: #1985e1; color: #fff;">Nostalgia</i>Game Boy Camera /themes/front/fraction/images from 2000 seem 10 times as old</strong>
							<img src="/themes/front/fraction/images/photos/image-8.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer fourth">
						<a href="post.html">
							<strong><i style="background-color: #429d4a; color: #fff;">Contests</i>Contest: Win one of two Watch Dogs duffle bags by Frank &amp; Oak</strong>
							<img src="/themes/front/fraction/images/photos/image-9.jpg" alt="" />
						</a>
					</div>
				<!-- END .ot-slide -->
				</div>

				<!-- BEGIN .ot-slide -->
				<div class="ot-slide">
					<div class="ot-slider-layer first">
						<a href="post.html">
							<strong><i style="background-color: #ed2d00; color: #fff;">Breaking News</i>a fight club on the top of the world</strong>
							<img src="/themes/front/fraction/images/photos/image-6.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer second">
						<a href="post.html">
							<strong><i style="background-color: #ffd800; color: #232323;">World News</i>SAMENWERKING GOLIATH SPORTSWEAR &amp; TWEE NEDERLANDSE PINTEREST INFLUENCERS</strong>
							<img src="/themes/front/fraction/images/photos/image-7.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer third">
						<a href="post.html">
							<strong><i style="background-color: #1985e1; color: #fff;">Nostalgia</i>Game Boy Camera /themes/front/fraction/images from 2000 seem 10 times as old</strong>
							<img src="/themes/front/fraction/images/photos/image-8.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer fourth">
						<a href="post.html">
							<strong><i style="background-color: #429d4a; color: #fff;">Contests</i>Contest: Win one of two Watch Dogs duffle bags by Frank &amp; Oak</strong>
							<img src="/themes/front/fraction/images/photos/image-9.jpg" alt="" />
						</a>
					</div>
				<!-- END .ot-slide -->
				</div>

				<!-- BEGIN .ot-slide -->
				<div class="ot-slide">
					<div class="ot-slider-layer first">
						<a href="post.html">
							<strong><i style="background-color: #ed2d00; color: #fff;">Breaking News</i>a fight club on the top of the world</strong>
							<img src="/themes/front/fraction/images/photos/image-6.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer second">
						<a href="post.html">
							<strong><i style="background-color: #ffd800; color: #232323;">World News</i>SAMENWERKING GOLIATH SPORTSWEAR &amp; TWEE NEDERLANDSE PINTEREST INFLUENCERS</strong>
							<img src="/themes/front/fraction/images/photos/image-7.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer third">
						<a href="post.html">
							<strong><i style="background-color: #1985e1; color: #fff;">Nostalgia</i>Game Boy Camera /themes/front/fraction/images from 2000 seem 10 times as old</strong>
							<img src="/themes/front/fraction/images/photos/image-8.jpg" alt="" />
						</a>
					</div>
					<div class="ot-slider-layer fourth">
						<a href="post.html">
							<strong><i style="background-color: #429d4a; color: #fff;">Contests</i>Contest: Win one of two Watch Dogs duffle bags by Frank &amp; Oak</strong>
							<img src="/themes/front/fraction/images/photos/image-9.jpg" alt="" />
						</a>
					</div>
				<!-- END .ot-slide -->
				</div>

			<!-- END .ot-slider -->
			</div>

			<!-- BEGIN .left-content -->
			<div class="left-content">

				<!-- BEGIN .home-block -->
				<div class="home-block">
					<div class="main-title">
						<a href="#" class="right button">View More Posts</a>
						<h2>Hottest news of today</h2>
						<span>Most recent news from all categories</span>
					</div>

					@if(isset($records))
					<!-- BEGIN .article-list-block -->
					<div class="article-list-block">
						@foreach($records as $record)
						<div class="item">
							<div class="item-header">
								<a href="post.html" class="image-hover" data-path-hover="M 37,53 45,34 58.5,37.5 61,59 48.5,69 z">
									<figure style="height:180px;">
										@if(!empty($record->img_src))
											<img src="{{ asset("uploads/$record->img_src") }}"  style="max-width:100%; max-height:100%;"/>
										@else
											<img src="/themes/front/fraction/images/photos/image-58.jpg" />
										@endif
										
										<svg viewBox="0 0 100 100" preserveAspectRatio="none"><path d="M 0,100 0,99 50,97 100,99 100,100 z" fill="#276197" /></svg>
										<figcaption>
											<span class="hover-text"><i class="fa fa-camera"></i><span>Photo</span></span>
										</figcaption>
									</figure>
								</a>
							</div>
							<div class="item-content">
								<div class="content-category">
									@foreach(Posts::find($record->id_post)->categories as $category)
									<a href="{{ URL::to('categories/'.$category->slug) }}" style="color: #276197;">{{$category->category}}</a>
									@endforeach
								</div>
								<h3><a href="{{ URL::to($record->slug) }}">{{ str_limit($record->title, 25, '...') }}</a></h3>
								<!-- <div class="ot-star-rating">
									<span style="width: 100%;" class=""><strong class="rating">5</strong> out of 5</span>
									<strong>Rating: 5 out of 5 stars</strong>
								</div> -->
								<span class="kanji-wrap" >{{ str_limit($record->content, 100, ' ...') }}</span>
								
							</div>
							<a href="{{ URL::to($record->slug) }}" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
						</div>
						@endforeach
					<!-- END .article-list-block -->
					</div>
					@endif
				<!-- END .home-block -->
				</div>

				<!-- BEGIN .home-block -->
				<div class="home-block">
					<div class="main-title" style="border-left: 4px solid #A161DD">
						<a href="#" class="right button" style="background: #A161DD; color: #A161DD;">View More Posts</a>
						<h2>Latest From Nostalgia</h2>
						<span>Most recent old school articles</span>
					</div>

					<!-- BEGIN .category-default-block -->
					<div class="category-default-block paragraph-row">

						<!-- BEGIN .column6 -->
						<div class="column6">
							<div class="item-main">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-81.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<div class="content-category">
										<a href="categroy.html" style="color: #A161DD;">Nostalgia</a>
									</div>
									<h3><a href="post-vimeo.html">Two Icelandic developers took me underground to discuss the future of VR</a></h3>
									<p>Modus novum oportere quo ex. No eos agam ludus ponderum, in pri justo facete, eos tollit ponderum torquatos ut. Meis verterem ullamcorper in per...</p>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						<!-- END .column6 -->
						</div>

						<!-- BEGIN .column6 -->
						<div class="column6 smaller-articles">

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-21.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Affert ignota persius vim solum suavitate est et in ius nonumy invenire indoctum</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-22.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Quo id  in habeo inimicus liber sonet pertinax ipsum facilis qui in</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-23.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Saepe veniam menandri ea has duo neglegentur no vix eu posse perpetua dissentiunt</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-79.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Affert ignota persius vim solum suavitate est et in ius nonumy invenire indoctum</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-80.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Quo id  in habeo inimicus liber sonet pertinax ipsum facilis qui in</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

						<!-- END .column6 -->
						</div>

					<!-- END .category-default-block -->
					</div>

				<!-- END .home-block -->
				</div>

				<!-- BEGIN .home-block -->
				<div class="home-block">
					<div class="main-title" style="border-left: 4px solid #429d4a">
						<a href="#" class="right button" style="background: #429d4a; color: #429d4a;">View More Posts</a>
						<h2>Latest From Contests</h2>
						<span>Most recent contest articles</span>
					</div>

					<!-- BEGIN .category-default-block -->
					<div class="category-default-block paragraph-row">

						<!-- BEGIN .column6 -->
						<div class="column6">
							<div class="item-main">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-78.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<div class="content-category">
										<a href="categroy.html" style="color: #429d4a;">Contests</a>
									</div>
									<h3><a href="post-vimeo.html">Two Icelandic developers took me underground to discuss the future of VR</a></h3>
									<p>Modus novum oportere quo ex. No eos agam ludus ponderum, in pri justo facete, eos tollit ponderum torquatos ut. Meis verterem ullamcorper in per...</p>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						<!-- END .column6 -->
						</div>

						<!-- BEGIN .column6 -->
						<div class="column6 smaller-articles">

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-21.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Affert ignota persius vim solum suavitate est et in ius nonumy invenire indoctum</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-22.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Quo id  in habeo inimicus liber sonet pertinax ipsum facilis qui in</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-23.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Saepe veniam menandri ea has duo neglegentur no vix eu posse perpetua dissentiunt</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-79.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Affert ignota persius vim solum suavitate est et in ius nonumy invenire indoctum</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

							<div class="item">
								<div class="item-header">
									<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-80.jpg" alt="" /></a>
								</div>
								<div class="item-content">
									<h3><a href="post-vimeo.html">Quo id  in habeo inimicus liber sonet pertinax ipsum facilis qui in</a></h3>
									<a href="post-vimeo.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>

						<!-- END .column6 -->
						</div>

					<!-- END .category-default-block -->
					</div>

				<!-- END .home-block -->
				</div>

				<!-- BEGIN .home-block -->
				<div class="home-block">

					<!-- BEGIN .article-links-block -->
					<div class="article-links-block">

						<div class="item">
							<h3 style="color: #ef8722; border-bottom: 3px solid #ef8722;">World News</h3>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Vis lobortis partiendo dissentiet quo</a></h3>
								<div class="item-details">
									<div class="item-head">
										<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-64.jpg" alt="" /></a>
									</div>
									<div class="item-content">
										<!-- <p>Aperiam argumentum eos an, no zril iuvaret aliquid per, laudem erroribus mel ex. Ea pro meis ocurreret volutpat ex...</p> -->
										<a href="post.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
									</div>
									<div class="clear-float"></div>
								</div>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci nec in altera eloquentiam ad nulla dolore</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Ullum liber virtute mei altera eloquentiam ad ut mutat pri ut</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Duo scripta an pri eirmod</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">In offendit probatus altera eloquentiam ad audire labores</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci altera eloquentiam ad ec in nulla dolore</a></h3>
							</div>
							<a href="category.html" class="archive-button" style="background-color: #ef8722;">More Articles</a>
						</div>

						<div class="item">
							<h3 style="color: #276197; border-bottom: 3px solid #276197;">Technology News</h3>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Vis lobortis partiendo dissentiet quo</a></h3>
								<div class="item-details">
									<div class="item-head">
										<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-65.jpg" alt="" /></a>
									</div>
									<div class="item-content">
										<!-- <p>Aperiam argumentum eos an, no zril iuvaret aliquid per, laudem erroribus mel ex. Ea pro meis ocurreret volutpat ex...</p> -->
										<a href="post.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
									</div>
									<div class="clear-float"></div>
								</div>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci nec in altera eloquentiam ad nulla dolore</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Ullum liber virtute mei altera eloquentiam ad ut mutat pri ut</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Duo scripta an pri eirmod</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">In offendit probatus altera eloquentiam ad audire labores</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci altera eloquentiam ad ec in nulla dolore</a></h3>
							</div>
							<a href="category.html" class="archive-button" style="background-color: #276197;">More Articles</a>
						</div>

						<div class="item">
							<h3 style="color: #6bab32; border-bottom: 3px solid #6bab32;">Nature</h3>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Vis lobortis partiendo dissentiet quo</a></h3>
								<div class="item-details">
									<div class="item-head">
										<a href="#" class="image-hover"><img src="/themes/front/fraction/images/photos/image-66.jpg" alt="" /></a>
									</div>
									<div class="item-content">
										<!-- <p>Aperiam argumentum eos an, no zril iuvaret aliquid per, laudem erroribus mel ex. Ea pro meis ocurreret volutpat ex...</p> -->
										<a href="post.html" class="read-more-link">Read More<i class="fa fa-angle-double-right"></i></a>
									</div>
									<div class="clear-float"></div>
								</div>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci nec in altera eloquentiam ad nulla dolore</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Ullum liber virtute mei altera eloquentiam ad ut mutat pri ut</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Duo scripta an pri eirmod</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">In offendit probatus altera eloquentiam ad audire labores</a></h3>
							</div>
							<div class="post-item">
								<span class="itemdate left">25.07.14</span>
								<h3><a href="#">Quo id wisi graeci altera eloquentiam ad ec in nulla dolore</a></h3>
							</div>
							<a href="category.html" class="archive-button" style="background-color: #6bab32;">More Articles</a>
						</div>

					<!-- END .article-links-block -->
					</div>

				<!-- END .home-block -->
				</div>

				

				<!-- BEGIN .home-block -->
				<div class="home-block">
					<div class="main-title" style="border-left: 4px solid #ef8722">
						<a href="#" class="right button" style="background: #ef8722; color: #ef8722;">View More Posts</a>
						<h2>Featured From World News</h2>
						<span>Most recent photo galleries</span>
					</div>
					<div class="home-featured-article">
						<a href="post.html" class="home-featured-item active">
							<span class="feature-text">
								<strong>Vocent saperet platonem mei eu</strong>
								<span>Et sit nobis alterum adversarium, qui ex elit integre, ea scribentur signiferumque usu. An duo stet etiam tritani, qui eruditi tibique iracundia an. Quidam delenit at vis, eam civibus menandri ut. Cu duo nisl probo dicunt.</span>
							</span>
							<img src="/themes/front/fraction/images/photos/image-71.jpg" alt="" />
						</a>
						<a href="post.html" class="home-featured-item">
							<span class="feature-text">
								<strong>Quidam delenit at vis eam</strong>
								<span>Debitis adolescens vim no, eu meis atqui eos, duo id dolorem deserunt hendrerit. Quo etiam mollis interesset in. Veniam appetere abhorreant ad cum, hinc viderer has an.</span>
							</span>
							<img src="/themes/front/fraction/images/photos/image-72.jpg" alt="" />
						</a>
						<a href="post.html" class="home-featured-item">
							<span class="feature-text">
								<strong>Vocent saperet platonem mei eu</strong>
								<span>Et sit nobis alterum adversarium, qui ex elit integre, ea scribentur signiferumque usu. An duo stet etiam tritani, qui eruditi tibique iracundia an. Quidam delenit at vis, eam civibus menandri ut. Cu duo nisl probo dicunt.</span>
							</span>
							<img src="/themes/front/fraction/images/photos/image-73.jpg" alt="" />
						</a>
						<a href="post.html" class="home-featured-item">
							<span class="feature-text">
								<strong>Quidam delenit at vis eam</strong>
								<span>Debitis adolescens vim no, eu meis atqui eos, duo id dolorem deserunt hendrerit. Quo etiam mollis interesset in. Veniam appetere abhorreant ad cum, hinc viderer has an.</span>
							</span>
							<img src="/themes/front/fraction/images/photos/image-74.jpg" alt="" />
						</a>
						<div class="home-featured-menu">
							<a href="#" class="active">1</a>
							<a href="#">2</a>
							<a href="#">3</a>
							<a href="#">4</a>
						</div>
					</div>
				<!-- END .home-block -->
				</div>
				
			<!-- END .left-content -->
			</div>

			@include('front.fraction.sidebar')


		</div>
		
	<!-- END .wrapper -->
	</div>
	
<!-- BEGIN .content -->
</section>

<script>
	jQuery(function(){

		$('.kanji-wrap').each(function(){

			var value = $(this).text();
			var regex = /\s+/gi;
			var words = value.trim().replace(regex, ' ').split(' ');
			var wrapped ="";
			for (var i in words) {
				wrapped = wrapped + "<span>"+words[i]+" </span>&nbsp;";
			}
			$(this).html('<p>'+wrapped+'</p>');
			
		});
		
	});
</script>
@stop