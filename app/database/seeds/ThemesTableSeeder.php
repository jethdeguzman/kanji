<?php

class ThemesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('themes')->truncate();
        
		\DB::table('themes')->insert(array (
			0 => 
			array (
				'id_theme' => '1',
				'theme' => 'marketing',
				'description' => 'Marketing Theme',
				'status' => '0',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 09:39:45',
			),
			1 => 
			array (
				'id_theme' => '2',
				'theme' => 'fraction',
				'description' => 'Fraction Theme',
				'status' => '1',
				'created_at' => '2014-12-02 09:53:58',
				'updated_at' => '2014-12-02 09:39:45',
			),
		));
	}

}
