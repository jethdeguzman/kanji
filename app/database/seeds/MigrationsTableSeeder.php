<?php

class MigrationsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('migrations')->truncate();
        
		\DB::table('migrations')->insert(array (
			0 => 
			array (
				'migration' => '2014_07_04_110545_create_users_table',
				'batch' => '1',
			),
			1 => 
			array (
				'migration' => '2014_07_04_115201_create_session_table',
				'batch' => '1',
			),
			2 => 
			array (
				'migration' => '2014_07_07_084357_create_posts_table',
				'batch' => '1',
			),
			3 => 
			array (
				'migration' => '2014_07_07_114248_create_settings_table',
				'batch' => '1',
			),
			4 => 
			array (
				'migration' => '2014_07_09_084340_create_carousels_table',
				'batch' => '1',
			),
			5 => 
			array (
				'migration' => '2014_07_10_054925_create_categories_table',
				'batch' => '1',
			),
			6 => 
			array (
				'migration' => '2014_07_11_074743_create_posts_categories_table',
				'batch' => '1',
			),
			7 => 
			array (
				'migration' => '2014_07_14_024319_create_roles_table',
				'batch' => '1',
			),
			8 => 
			array (
				'migration' => '2014_07_14_071558_create_permissions_table',
				'batch' => '1',
			),
			9 => 
			array (
				'migration' => '2014_07_14_074133_create_roles_permissions_table',
				'batch' => '1',
			),
			10 => 
			array (
				'migration' => '2014_07_17_023136_create_users_permissions_table',
				'batch' => '1',
			),
			11 => 
			array (
				'migration' => '2014_07_18_085503_create_themes_table',
				'batch' => '1',
			),
			12 => 
			array (
				'migration' => '2014_12_02_083630_add_slug_to_categories_table',
				'batch' => '2',
			),
		));
	}

}
