<?php

class PermissionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('permissions')->truncate();
        
		\DB::table('permissions')->insert(array (
			0 => 
			array (
				'id_permission' => '1',
				'permission' => 'Create',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			1 => 
			array (
				'id_permission' => '2',
				'permission' => 'View',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			2 => 
			array (
				'id_permission' => '3',
				'permission' => 'Edit',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			3 => 
			array (
				'id_permission' => '4',
				'permission' => 'Delete',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			4 => 
			array (
				'id_permission' => '5',
				'permission' => 'None',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
		));
	}

}
