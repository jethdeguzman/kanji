<?php

class CategoriesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('categories')->truncate();
        
		\DB::table('categories')->insert(array (
			0 => 
			array (
				'id_category' => '1',
				'category' => 'Uncategorized',
				'description' => 'uncategorized',
				'status' => '0',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
				'slug' => 'uncategorized',
			),
			1 => 
			array (
				'id_category' => '2',
				'category' => 'Tech News ',
				'description' => 'News from latest in tech industry',
				'status' => '1',
				'created_at' => '2014-12-02 06:14:02',
				'updated_at' => '2014-12-02 09:50:20',
				'slug' => 'tech-news',
			),
			2 => 
			array (
				'id_category' => '3',
				'category' => 'World News',
				'description' => 'News from different part of the world.',
				'status' => '1',
				'created_at' => '2014-12-02 06:14:17',
				'updated_at' => '2014-12-02 09:51:43',
				'slug' => 'world-news',
			),
			3 => 
			array (
				'id_category' => '4',
				'category' => 'Political News',
				'description' => '',
				'status' => '1',
				'created_at' => '2014-12-02 06:14:31',
				'updated_at' => '2014-12-02 06:14:50',
				'slug' => 'political-news',
			),
			4 => 
			array (
				'id_category' => '5',
				'category' => 'Sports',
				'description' => '',
				'status' => '1',
				'created_at' => '2014-12-02 06:14:42',
				'updated_at' => '2014-12-02 06:14:52',
				'slug' => 'sports',
			),
			5 => 
			array (
				'id_category' => '6',
				'category' => 'Nature',
				'description' => '',
				'status' => '1',
				'created_at' => '2014-12-02 06:15:01',
				'updated_at' => '2014-12-02 06:15:08',
				'slug' => 'nature',
			),
			6 => 
			array (
				'id_category' => '7',
				'category' => 'Health News',
				'description' => '',
				'status' => '1',
				'created_at' => '2014-12-02 08:41:38',
				'updated_at' => '2014-12-02 08:41:38',
				'slug' => 'health-news',
			),
		));
	}

}
