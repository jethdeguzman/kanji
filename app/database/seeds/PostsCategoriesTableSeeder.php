<?php

class PostsCategoriesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('posts_categories')->truncate();
        
		\DB::table('posts_categories')->insert(array (
			0 => 
			array (
				'id_post_category' => '14',
				'id_post' => '3',
				'id_category' => '2',
				'created_at' => '2014-12-02 06:16:04',
				'updated_at' => '2014-12-02 06:16:04',
			),
			1 => 
			array (
				'id_post_category' => '18',
				'id_post' => '4',
				'id_category' => '3',
				'created_at' => '2014-12-02 07:11:48',
				'updated_at' => '2014-12-02 07:11:48',
			),
			2 => 
			array (
				'id_post_category' => '19',
				'id_post' => '5',
				'id_category' => '4',
				'created_at' => '2014-12-02 10:31:24',
				'updated_at' => '2014-12-02 10:31:24',
			),
			3 => 
			array (
				'id_post_category' => '20',
				'id_post' => '6',
				'id_category' => '7',
				'created_at' => '2014-12-02 10:33:06',
				'updated_at' => '2014-12-02 10:33:06',
			),
			4 => 
			array (
				'id_post_category' => '21',
				'id_post' => '7',
				'id_category' => '2',
				'created_at' => '2014-12-02 10:34:26',
				'updated_at' => '2014-12-02 10:34:26',
			),
			5 => 
			array (
				'id_post_category' => '22',
				'id_post' => '8',
				'id_category' => '2',
				'created_at' => '2014-12-02 10:36:49',
				'updated_at' => '2014-12-02 10:36:49',
			),
		));
	}

}
