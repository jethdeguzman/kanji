<?php

class PostsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('posts')->truncate();
        
		\DB::table('posts')->insert(array (
			0 => 
			array (
				'id_post' => '1',
				'post_type' => 'page',
				'title' => 'Blog',
				'slug' => 'blog',
				'content' => 'Lorem ipsum dolor sit amet.',
				'img_src' => NULL,
				'post_cat' => '1',
				'status' => '0',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			1 => 
			array (
				'id_post' => '2',
				'post_type' => 'post',
				'title' => 'Lorem ipsum dolor sit amet',
				'slug' => 'lorem-ipsum-dolor-sit-amet',
				'content' => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
				'img_src' => NULL,
				'post_cat' => '1',
				'status' => '0',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 06:30:21',
			),
			2 => 
			array (
				'id_post' => '3',
				'post_type' => 'post',
				'title' => 'Kanji',
				'slug' => 'kanji',
				'content' => '<p>My fellow 米mericans, 総upreme 盟llied 帥ommander 将eneral MacArthur, and 盟llied representatives on the 艦attleship Missouri in Tokyo 湾ay:</p><p>The 想houghts and 望opes of 全ll 米merica, 実ndeed of 全ll the civilized 世orld, are 央entered 晩onight on the 艦attleship Missouri. There on that 小mall 個iece of 米merican 土oil 碇nchored in Tokyo 港arbor the Japanese have just 公fficially laid-down 彼heir 武rms. 彼hey have 署igned terms of unconditional 諦urrender.</p><p>四our 年ears 前go, the 想houghts and 恐ears of the 全hole civilized 世orld were 央entered on 別nother 個iece of 米merican 土oil, 珠earl 港arbor. The 強ighty 脅hreat to civilization 何hich 初egan 有here-is 今ow laid at 休est. It was a 永ong 路oad to Tokyo, and a 血loody one.</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 03:36:48',
				'updated_at' => '2014-12-02 06:16:04',
			),
			3 => 
			array (
				'id_post' => '4',
				'post_type' => 'post',
				'title' => 'Erant aeque ne ius nonumes electram ad nam',
				'slug' => 'erant-aeque-ne-ius-nonumes-electram-ad-nam',
				'content' => '<p>We shall 不ot 忘orget 珠earl 港arbor.&nbsp;The Japanese 軍ilitarists will 不ot 忘orget the U.S.S. Missouri.</p><p>The 悪vil done by the Japanese 戦ar lords can 不ever be 修epaired or 忘orgotten. But 彼heir 力ower to 破estroy and 殺ill has been 取aken from 彼hem. 彼heir 軍rmies and 何hat is 残eft of 彼heir Navy are 今ow impotent.</p><p>To 全ll of us there comes 初irst a 覚ense of 謝ratitude to Almighty-God 誰ho sustained us and 我ur 盟llies in the 暗ark 日ays of 重rave 危anger, 誰ho made us to 培row from 劣eakness into the 強trongest 戦ighting 軍orce in 史istory, and 誰ho has 今ow 覧een us 超vercome the 軍orces of tyranny that 求ought to 破estroy 彼is civilization.&nbsp;</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 06:31:34',
				'updated_at' => '2014-12-02 07:11:48',
			),
			4 => 
			array (
				'id_post' => '5',
				'post_type' => 'post',
				'title' => 'Lorem Ipsum',
				'slug' => 'lorem-ipsum',
				'content' => '<p>To 全ll of us there comes 初irst a 覚ense of 謝ratitude to Almighty-God 誰ho sustained us and 我ur 盟llies in the 暗ark 日ays of 重rave 危anger, 誰ho made us to 培row from 劣eakness into the 強trongest 戦ighting 軍orce in 史istory, and 誰ho has 今ow 覧een us 超vercome the 軍orces of tyranny that 求ought to 破estroy 彼is civilization.</p><p>神od 与rant that in 我ur 誇ride of 今his 時our, we may 不ot 忘orget the 難ard tasks that are still 前efore us; that we may 寄pproach these with the 同ame 勇ourage, zeal, and 耐atience with 何hich we 向aced the 試rials and 困roblems of the past 四our 年ears.</p><p>我ur 初irst 意houghts, of-course -- 意houghts of 謝ratefulness and 深eep obligation -- go out to 彼hose of 我ur 愛oved ones 誰ho have been 殺illed or maimed in this 凄errible 戦ar. On 地and and 海ea and in the 空ir, 米merican 男en and 婦omen have 捧iven 彼heir 命ives so that 今his 日ay of 最ltimate 勝ictory might come and 確ssure the survival of a civilized 世orld. No 勝ictory can make good 彼heir 喪oss.</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 10:31:24',
				'updated_at' => '2014-12-02 10:31:24',
			),
			5 => 
			array (
				'id_post' => '6',
				'post_type' => 'post',
				'title' => 'Dolor Sit Amet',
				'slug' => 'dolor-sit-amet',
				'content' => '<p>We 思hink of 彼hose 誰hom 死eath in this 戦ar has 傷urt, 取aking from 彼hem 父athers, 夫usbands, 息ons, 兄rothers, and 妹isters 誰hom 彼hey 愛oved. No 勝ictory can bring back the 顔aces 彼hey 永ong to 覧ee.</p><p>只nly the 知nowledge that the 勝ictory, 何hich these 犠acrifices have made 可ossible, will-be 智isely 使sed can 与ive 彼hem 何ny 慰omfort. It is 我ur 責esponsibility &ndash; 我urs, the 命iving &ndash; to 覧ee to it that this 勝ictory shall be a 碑onument 価orthy of the 死ead 誰ho 死ied to 勝in it. [audio gap, content uncertain].</p><p>全ll the millions of 男en and 婦omen in 我ur 軍rmed-forces and 商erchant 海arine all-over the 世orld 誰ho, 後fter 年ears of 犠acrifice and 難ardship and 危eril, have been spared by Providence from 痛arm.</p><p>We 思hink of 全ll the 男en and 婦omen and 児hildren 誰ho 間uring these 年ears have carried on at 宅ome, in 寂onesomeness and 憂nxiety and 恐ear.</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 10:33:06',
				'updated_at' => '2014-12-02 10:33:06',
			),
			6 => 
			array (
				'id_post' => '7',
				'post_type' => 'post',
				'title' => 'Lorem Ipsum dolor ismet',
				'slug' => 'lorem-ipsum-dolor-ismet',
				'content' => '<p>我ur 意houghts go out to the millions of 米merican 労orkers and 商usiness 男en, to 我ur 農armers and 鉱iners, to 全ll 彼hose 誰ho have 建uilt up this 国ountry&rsquo;s 戦ighting 強trength, and 誰ho have 輸hipped to 我ur 盟llies the 資eans to 抗esist and 超vercome the 敵nemy.</p><p>我ur 意houghts go out to 我ur 官ivil-servants and to the 千housands of 米mericans 誰ho, at 私ersonal 犠acrifice, have come-to 働erve in 我ur 政overnment 間uring these 試rying 年ears, to the 員embers of the Selective Service-boards and ration boards, to the civilian 守efense and 赤ed 十ross 労orkers, to the 男en and 婦omen in the USO, and in the 興ntertainment 世orld, to 全ll 彼hose 誰ho have 助elped in this 協ooperative 戦truggle to 保reserve liberty and decency in the 世orld.</p><p>We 思hink of 我ur 霊eparted gallant 導eader, Franklin D. Roosevelt, 護efender of democracy, 築rchitect of 世orld 和eace and 協ooperation.</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 10:34:26',
				'updated_at' => '2014-12-02 10:34:26',
			),
			7 => 
			array (
				'id_post' => '8',
				'post_type' => 'post',
				'title' => 'Mel an modo debitis, quo id wisi graeci nec in nulla dolore',
				'slug' => 'mel-an-modo-debitis-quo-id-wisi-graeci-nec-in-nulla-dolore',
				'content' => '<p>And 我ur 意houghts go out to 我ur gallant 盟llies in this 戦ar, to 彼hose 誰ho 抗esisted the invaders, to 彼hose 誰ho were 不ot 強trong enough to hold out, but 誰ho, nevertheless, kept the 火ires of 抗esistance 生live 内ithin the 霊ouls of 彼heir 民eople, to 彼hose 誰ho 起tood-up 対gainst great odds and held the 線ine, 迄ntil the United Nations 共ogether were 可ble to 与upply the 武rms and the 男en with 何hich to 超vercome the 軍orces of 悪vil.</p><p>This is a 勝ictory of more-than arms 独lone. This is a 勝ictory of liberty over tyranny.</p><p>From 我ur 戦ar plants 回olled the tanks and planes 何hich 爆lasted 彼heir way to the 央eart of 我ur 敵nemies. From 我ur shipyards 跳prang the 艦hips 何hich 橋ridged 全ll the 海ceans of the 世orld for 我ur 武eapons and 給upplies. From 我ur 農arms came the 飯ood and 維iber for 我ur 軍rmies and navies and for 我ur 盟llies in 全ll the 角orners of the Earth. From 我ur mines and factories came the 生aw 料aterials and the 完inished 品roducts 何hich 与ave us the 器quipment to 超vercome 我ur 敵nemies.</p>',
				'img_src' => NULL,
				'post_cat' => '',
				'status' => '1',
				'created_at' => '2014-12-02 10:36:49',
				'updated_at' => '2014-12-02 10:36:49',
			),
		));
	}

}
