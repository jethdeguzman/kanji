<?php

class RolesPermissionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('roles_permissions')->truncate();
        
		\DB::table('roles_permissions')->insert(array (
			0 => 
			array (
				'id_role_permission' => '1',
				'id_role' => '1',
				'id_permission' => '1',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			1 => 
			array (
				'id_role_permission' => '2',
				'id_role' => '1',
				'id_permission' => '2',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			2 => 
			array (
				'id_role_permission' => '3',
				'id_role' => '1',
				'id_permission' => '3',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			3 => 
			array (
				'id_role_permission' => '4',
				'id_role' => '1',
				'id_permission' => '4',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			4 => 
			array (
				'id_role_permission' => '5',
				'id_role' => '2',
				'id_permission' => '1',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			5 => 
			array (
				'id_role_permission' => '6',
				'id_role' => '2',
				'id_permission' => '2',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
		));
	}

}
