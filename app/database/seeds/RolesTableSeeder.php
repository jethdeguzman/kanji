<?php

class RolesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('roles')->truncate();
        
		\DB::table('roles')->insert(array (
			0 => 
			array (
				'id_role' => '1',
				'role' => 'Admin',
				'description' => 'This role allows you to have full access.',
				'status' => '1',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			1 => 
			array (
				'id_role' => '2',
				'role' => 'Subscriber',
				'description' => 'This role create post/pages/categories',
				'status' => '1',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
		));
	}

}
