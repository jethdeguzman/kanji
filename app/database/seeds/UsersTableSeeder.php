<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users')->truncate();
        
		\DB::table('users')->insert(array (
			0 => 
			array (
				'id_user' => '1',
				'firstname' => 'John',
				'lastname' => 'Doe',
				'email' => 'admin@admin.com',
				'img_src' => NULL,
				'password' => '$2y$10$gkR71aUFkLLEDbLCl5pwUOhocgUxvmbT.bd8UiGX1LixrDhBbg0Ye',
				'status' => '1',
				'id_role' => '1',
				'remember_token' => NULL,
				'password_reset' => '$2y$10$wX5/bkr3QwDmsrpnJo4OsOJVRwMK6URsVbHCW8PH1WcV7FP1wwY0e',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
		));
	}

}
