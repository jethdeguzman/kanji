<?php

class SettingsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('settings')->truncate();
        
		\DB::table('settings')->insert(array (
			0 => 
			array (
				'id_settings' => '1',
				'settings_name' => 'POST_IN_PAGE',
				'settings_value' => 'blog',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			1 => 
			array (
				'id_settings' => '2',
				'settings_name' => 'WEBSITE_NAME',
				'settings_value' => 'xFinity CMS',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			2 => 
			array (
				'id_settings' => '3',
				'settings_name' => 'POSTS_PER_PAGE',
				'settings_value' => '10',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			3 => 
			array (
				'id_settings' => '4',
				'settings_name' => 'ORDER_BY',
				'settings_value' => 'created_at',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
			4 => 
			array (
				'id_settings' => '5',
				'settings_name' => 'ARRANGE_BY',
				'settings_value' => 'DESC',
				'created_at' => '2014-12-02 01:52:27',
				'updated_at' => '2014-12-02 01:52:27',
			),
		));
	}

}
