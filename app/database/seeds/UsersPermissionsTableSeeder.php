<?php

class UsersPermissionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('users_permissions')->truncate();
        
		\DB::table('users_permissions')->insert(array (
			0 => 
			array (
				'id_user_permission' => '1',
				'id_user' => '1',
				'id_permission' => '1',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			1 => 
			array (
				'id_user_permission' => '2',
				'id_user' => '1',
				'id_permission' => '2',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			2 => 
			array (
				'id_user_permission' => '3',
				'id_user' => '1',
				'id_permission' => '3',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
			3 => 
			array (
				'id_user_permission' => '4',
				'id_user' => '1',
				'id_permission' => '4',
				'created_at' => '2014-12-02 01:52:28',
				'updated_at' => '2014-12-02 01:52:28',
			),
		));
	}

}
