<?php

class FrontController extends BaseController {

	public function __construct()
	{
		parent::__construct();

		View::share(array(
				'pages' 			=> Posts::pages()->orderBy('created_at', 'DESC')->get()
			));
		
		$this->layout = 'front.'.$this->theme->theme.'.tpl.main';
	}

	public function viewSlug()
	{
		$theme						= Themes::active()->first();
		/*RETURN THE PAGE BEING VIEWED*/
		$data['record']				= Posts::view(Request::segment(1));
		$this->layout->content 		= View::make('front.'.$this->theme->theme.'.page', $data);
	}

	public function category()
	{
		$theme					= Themes::active()->first();
		$category				= Categories::where('slug', Request::segment(2));
		$data['category']   = null;
		$data['posts']      = null;
		
		if( $category->count() > 0 ) :
			
			$data['category']   = $category->first();
			$data['posts']      = Categories::find($category->first()->id_category)->posts()->orderby('created_at', 'DESC')->paginate(10);

		endif;

		$this->layout->content 		= View::make('front.'.$this->theme->theme.'.category', $data);
	}

	public function search()
	{
		$term = Input::get('term');

		if( Input::has('term') ) :

			$records  					= Posts::whereRaw('match(title, content) against (? in boolean mode)', [$term])->active()->paginate(10);		
			if ( count($records) > 0 ) :
				$data['records']		= $records;
			else :
				$data['error']			= 'No result found.';
			endif;
		
			$this->layout->content 		= View::make('front.'.$this->theme->theme.'.search', $data);
		
		else : 
			
			$data['error'] 				= 'Sorry you have not type any search term. Please try again.';
			$this->layout->content 		= View::make('front.'.$this->theme->theme.'.search', $data);
		
		endif;

	}
}
