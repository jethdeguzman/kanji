$(document).ready(function(){
	//POP UP A CONFIRM BOX ON A DELETE LINK
	$('#delete a.btn-danger').on('click', function(){
		
		var danger = confirm("Are you sure you want to delete?");
		
		if ( danger == true) {
		    return true;
		} else {
		    return false;
		}
	});	

	$('#status a.btn-danger').on('click', function(){
		
		var danger = confirm("Enable?");
		
		if ( danger == true) {
		    return true;
		} else {
		    return false;
		}
	});

	$('#status a.btn-success').on('click', function(){
		
		var danger = confirm("Disable?");
		
		if ( danger == true) {
		    return true;
		} else {
		    return false;
		}
	});	
});